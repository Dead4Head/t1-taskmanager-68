package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

import java.util.List;

public final class TaskListResponse extends AbstractTaskListResponse {

    public TaskListResponse() {
    }


    public TaskListResponse(@Nullable final List<TaskDTO> tasks) {
        super(tasks);
    }

    public TaskListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}