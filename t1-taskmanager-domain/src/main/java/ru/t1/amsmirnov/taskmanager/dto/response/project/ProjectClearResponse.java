package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class ProjectClearResponse extends AbstractResultResponse {

    public ProjectClearResponse() {
    }

    public ProjectClearResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}