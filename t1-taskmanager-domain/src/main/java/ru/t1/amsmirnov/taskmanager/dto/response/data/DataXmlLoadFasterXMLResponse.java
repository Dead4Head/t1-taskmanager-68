package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataXmlLoadFasterXMLResponse extends AbstractResultResponse {

    public DataXmlLoadFasterXMLResponse() {
    }

    public DataXmlLoadFasterXMLResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
