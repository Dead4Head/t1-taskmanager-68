package ru.t1.amsmirnov.taskmanager.api.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface IClientListener {

    @Nullable String getArgument();

    @Nullable String getDescription();

    @Nullable String getName();

    @Nullable Role[] getRoles();

    void handler(@NotNull final ConsoleEvent event) throws AbstractException;

}
