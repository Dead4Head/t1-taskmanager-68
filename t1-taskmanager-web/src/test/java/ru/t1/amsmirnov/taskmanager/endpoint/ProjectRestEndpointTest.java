package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.client.ProjectClient;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(DBCategory.class)
public class ProjectRestEndpointTest {

    private static final String BASE_URL = "http://localhost:8080/api/projects";
    private static final String TEST_PREFIX = "DATABASE_TEST_";
    private static final int TEST_PROJECTS_COUNT = 3;
    private static final List<ProjectWebDto> PROJECTS = new ArrayList<>();

    private final ProjectClient client = ProjectClient.client(BASE_URL);

    @Before
    public void initTest() {
        for (int i = 0; i < TEST_PROJECTS_COUNT; i++) {
            ProjectWebDto project = new ProjectWebDto(TEST_PREFIX + i);
            client.save(project);
            PROJECTS.add(project);
        }
    }

    @After
    public void clean() {
        PROJECTS.clear();
        client.deleteAll();
    }

    @Test
    public void testFindAll() {
        Collection<ProjectWebDto> testFind = client.findAll();
        Assert.assertEquals(PROJECTS.size(), testFind.size());
    }

    @Test
    public void testFindById() {
        ProjectWebDto project = client.findById(PROJECTS.get(0).getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), PROJECTS.get(0).getId());
    }

    @Test
    public void testDelete() {
        @NotNull final String id = PROJECTS.get(0).getId();
        client.delete(PROJECTS.get(0));
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testDeleteById() {
        @NotNull final String id = PROJECTS.get(0).getId();
        client.delete(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testSave() {
        @NotNull final ProjectWebDto project = new ProjectWebDto(TEST_PREFIX + "TEST_SAVE");
        client.save(project);
        Assert.assertNotNull(project);
        String projectId = project.getId();
        ProjectWebDto projectTest = client.findById(projectId);
        Assert.assertNotNull(projectTest);
        client.delete(project);
    }

}
