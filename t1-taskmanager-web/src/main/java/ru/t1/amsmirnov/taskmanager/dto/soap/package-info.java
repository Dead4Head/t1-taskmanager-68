@javax.xml.bind.annotation.XmlSchema(
        namespace = "http://t1.ru/amsmirnov/taskmanager/dto/soap",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)
package ru.t1.amsmirnov.taskmanager.dto.soap;
