package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;

import javax.xml.bind.annotation.*;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveResponse")
public class ProjectSaveResponse {

    @XmlElement(required = true)
    protected ProjectWebDto project;

    public ProjectSaveResponse() {
    }

    public ProjectSaveResponse(ProjectWebDto project) {
        this.project = project;
    }

    public ProjectWebDto getProject() {
        return project;
    }

    public void setProject(ProjectWebDto value) {
        this.project = value;
    }

}
