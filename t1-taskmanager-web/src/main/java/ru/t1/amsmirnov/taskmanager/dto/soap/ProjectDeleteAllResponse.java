package ru.t1.amsmirnov.taskmanager.dto.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectDeleteAllResponse")
public class ProjectDeleteAllResponse {

    public ProjectDeleteAllResponse() {

    }

}
