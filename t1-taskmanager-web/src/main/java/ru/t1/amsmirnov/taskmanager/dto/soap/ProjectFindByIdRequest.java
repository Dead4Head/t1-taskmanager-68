package ru.t1.amsmirnov.taskmanager.dto.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindByIdRequest")
public class ProjectFindByIdRequest {

    @XmlElement(required = true)
    protected String id;

    public ProjectFindByIdRequest() {
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

}
