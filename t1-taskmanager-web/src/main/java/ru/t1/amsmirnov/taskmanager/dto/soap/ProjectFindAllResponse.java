package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindAllResponse")
public class ProjectFindAllResponse {

    protected List<ProjectWebDto> projects;

    public ProjectFindAllResponse() {
    }

    public ProjectFindAllResponse(List<ProjectWebDto> projects) {
        this.projects = projects;
    }

    public List<ProjectWebDto> getProjects() {
        return this.projects;
    }

    public void setProjects(List<ProjectWebDto> projects) {
        this.projects = projects;
    }

}
