package ru.t1.amsmirnov.taskmanager.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.amsmirnov.taskmanager.dto.soap.*;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

@Endpoint
public class TaskSoapEndpoint {

    public static final String LOCATION_URI = "/ws";
    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";
    public static final String NAMESPACE = "http://t1.ru/amsmirnov/taskmanager/dto/soap";

    @Autowired
    private TaskDtoService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(
            @RequestPayload final TaskFindAllRequest request
    ) {
        try {
            return new TaskFindAllResponse(taskService.findAll());
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(
            @RequestPayload final TaskFindByIdRequest request
    ) {
        try {
            return new TaskFindByIdResponse(taskService.findOneById(request.getId()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(
            @RequestPayload final TaskSaveRequest request
    ) {
        try {
            return new TaskSaveResponse(taskService.update(request.getTask()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(
            @RequestPayload final TaskDeleteRequest request
    ) {
        try {
            return new TaskDeleteResponse(taskService.removeOne(request.getTask()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse delete(
            @RequestPayload final TaskDeleteByIdRequest request
    ) {
        try {
            return new TaskDeleteByIdResponse(taskService.removeOneById(request.getId()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(
            @RequestPayload final TaskDeleteAllRequest request
    ) {
        try {
            taskService.removeAll();
            return new TaskDeleteAllResponse();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
