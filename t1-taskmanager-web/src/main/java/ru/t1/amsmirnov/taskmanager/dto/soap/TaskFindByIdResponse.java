package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindByIdResponse")
public class TaskFindByIdResponse {

    protected TaskWebDto task;

    public TaskFindByIdResponse() {
    }

    public TaskFindByIdResponse(TaskWebDto task) {
        this.task = task;
    }

    public TaskWebDto getTask() {
        return task;
    }

    public void setTask(TaskWebDto value) {
        this.task = value;
    }

}
