package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import javax.xml.bind.annotation.*;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteRequest")
public class TaskDeleteRequest {

    @XmlElement(required = true)
    protected TaskWebDto task;

    public TaskDeleteRequest() {
    }

    public TaskWebDto getTask() {
        return task;
    }

    public void setTask(TaskWebDto value) {
        this.task = value;
    }

}
