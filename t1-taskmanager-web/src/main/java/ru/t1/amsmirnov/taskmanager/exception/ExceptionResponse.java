package ru.t1.amsmirnov.taskmanager.exception;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public class ExceptionResponse extends AbstractResultResponse {

    private String status = HttpStatus.BAD_REQUEST.getReasonPhrase();
    private int statusCode = HttpStatus.BAD_REQUEST.value();
    
    public ExceptionResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    public ExceptionResponse(@NotNull final HttpStatus status, @NotNull final Throwable throwable) {
        super(throwable);
        this.status = status.getReasonPhrase();
        this.statusCode = status.value();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

}
