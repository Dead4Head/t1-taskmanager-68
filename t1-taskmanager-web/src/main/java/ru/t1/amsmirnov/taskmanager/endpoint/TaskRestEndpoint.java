package ru.t1.amsmirnov.taskmanager.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.amsmirnov.taskmanager.api.ITaskRestEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks", produces = "application/json")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskDtoService taskService;

    @Override
    @GetMapping("/findAll")
    public List<TaskWebDto> findAll() {
        try {
            return taskService.findAll();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskWebDto findById(@PathVariable("id") final String id) {
        try {
            return taskService.findOneById(id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @PostMapping("/save")
    public TaskWebDto save(@RequestBody final TaskWebDto task) {
        try {
            return taskService.update(task);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/delete")
    public TaskWebDto delete(@RequestBody final TaskWebDto task) {
        try {
            return taskService.removeOne(task);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public TaskWebDto delete(@PathVariable("id") final String id) {
        try {
            return taskService.removeOneById(id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        try {
            taskService.removeAll();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
