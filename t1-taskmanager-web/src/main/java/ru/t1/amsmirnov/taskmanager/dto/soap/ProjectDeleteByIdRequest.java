package ru.t1.amsmirnov.taskmanager.dto.soap;

import javax.xml.bind.annotation.*;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectDeleteByIdRequest")
public class ProjectDeleteByIdRequest {

    @XmlElement(required = true)
    protected String id;

    public ProjectDeleteByIdRequest() {

    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

}
