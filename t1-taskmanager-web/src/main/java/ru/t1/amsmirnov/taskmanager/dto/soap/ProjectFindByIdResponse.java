package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindByIdResponse")
public class ProjectFindByIdResponse {

    protected ProjectWebDto project;

    public ProjectFindByIdResponse() {
    }

    public ProjectFindByIdResponse(ProjectWebDto project) {
        this.project = project;
    }

    public ProjectWebDto getProject() {
        return project;
    }

    public void setProject(ProjectWebDto value) {
        this.project = value;
    }

}
