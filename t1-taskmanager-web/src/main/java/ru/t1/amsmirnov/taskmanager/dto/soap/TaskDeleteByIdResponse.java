package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import javax.xml.bind.annotation.*;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteByIdResponse")
public class TaskDeleteByIdResponse {

    @XmlElement(required = true)
    protected TaskWebDto task;

    public TaskDeleteByIdResponse() {
    }

    public TaskDeleteByIdResponse(TaskWebDto task) {
        this.task = task;
    }

    public TaskWebDto getTask() {
        return task;
    }

    public void setTask(TaskWebDto value) {
        this.task = value;
    }

}
