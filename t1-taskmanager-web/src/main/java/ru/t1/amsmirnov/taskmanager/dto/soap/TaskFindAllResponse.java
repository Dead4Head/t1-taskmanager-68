package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindAllResponse")
public class TaskFindAllResponse {

    protected List<TaskWebDto> tasks;

    public TaskFindAllResponse() {
    }

    public TaskFindAllResponse(List<TaskWebDto> tasks) {
        this.tasks = tasks;
    }

    public List<TaskWebDto> getTasks() {
        return this.tasks;
    }

    public void setTasks(List<TaskWebDto> tasks) {
        this.tasks = tasks;
    }

}
