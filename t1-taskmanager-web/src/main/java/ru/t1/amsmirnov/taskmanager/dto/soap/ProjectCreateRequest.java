package ru.t1.amsmirnov.taskmanager.dto.soap;

import javax.xml.bind.annotation.*;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectCreateRequest")
public class ProjectCreateRequest {

    @XmlElement(required = true)
    protected String name;

    @XmlElement
    protected String description;

    public ProjectCreateRequest() {

    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

}
