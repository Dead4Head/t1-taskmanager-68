package ru.t1.amsmirnov.taskmanager.dto.soap;

import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;

import javax.xml.bind.annotation.*;

@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectDeleteByIdResponse")
public class ProjectDeleteByIdResponse {

    @XmlElement(required = true)
    protected ProjectWebDto project;

    public ProjectDeleteByIdResponse() {

    }

    public ProjectDeleteByIdResponse(ProjectWebDto project) {
        this.project = project;
    }

    public ProjectWebDto getProject() {
        return project;
    }

    public void setProject(ProjectWebDto value) {
        this.project = value;
    }

}
