package ru.t1.amsmirnov.taskmanager.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.amsmirnov.taskmanager.dto.soap.*;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;

@Endpoint
public class ProjectSoapEndpoint {

    public static final String LOCATION_URI = "/ws";
    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";
    public static final String NAMESPACE = "http://t1.ru/amsmirnov/taskmanager/dto/soap";

    @Autowired
    private ProjectDtoService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(
            @RequestPayload final ProjectCreateRequest request
    ) {
        try {
            return new ProjectCreateResponse(projectService.create(request.getName(), request.getDescription()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(
            @RequestPayload final ProjectFindAllRequest request
    ) {
        try {
            return new ProjectFindAllResponse(projectService.findAll());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(
            @RequestPayload final ProjectFindByIdRequest request
    ) {
        try {
            return new ProjectFindByIdResponse(projectService.findOneById(request.getId()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }


    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(
            @RequestPayload final ProjectSaveRequest request
    ) {
        try {
            return new ProjectSaveResponse(projectService.update(request.getProject()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }


    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(
            @RequestPayload final ProjectDeleteRequest request
    ) {
        try {
            return new ProjectDeleteResponse(projectService.removeOne(request.getProject()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }


    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse delete(
            @RequestPayload final ProjectDeleteByIdRequest request
    ) {
        try {
            return new ProjectDeleteByIdResponse(projectService.removeOneById(request.getId()));
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }


    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(
            @RequestPayload final ProjectDeleteAllRequest request
    ) {
        try {
            projectService.removeAll();
            return new ProjectDeleteAllResponse();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
