package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.List;
import java.util.Optional;


@NoRepositoryBean
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<M> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<M> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<M> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
