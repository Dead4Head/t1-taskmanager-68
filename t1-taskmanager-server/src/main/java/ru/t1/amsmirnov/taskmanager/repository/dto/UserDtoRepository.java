package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

import java.util.Optional;

public interface UserDtoRepository extends AbstractDtoRepository<UserDTO> {

    @NotNull
    Optional<UserDTO> findByLogin(@Nullable final String login);

    @NotNull
    Optional<UserDTO> findByEmail(@Nullable final String email);

    boolean existsByLogin(@Nullable final String login);

    boolean existsByEmail(@Nullable final String email);

}
