package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.IReceiverService;

import javax.jms.*;

@Service
public final class ReceiverService implements IReceiverService {

    @NotNull
    static final String QUEUE_NAME = "LOGGER";

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    public ReceiverService() {
    }

    @Override
    public void receive(@NotNull final MessageListener listener) throws JMSException {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE_NAME);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
